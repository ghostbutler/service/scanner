Scanner service
===============

Introduction
------------

This service will provide a way to scan for known devices/services.

Controller for each service type will have to register to scanner and specify
what kind of devices they are willing to receive by providing rules of detection.

Dependencies
------------

To get the go dependencies, type

```bash
go get gitlab.com/ghostbutler/tool/crypto
go get gitlab.com/ghostbutler/tool/service/keystore
go get gitlab.com/ghostbutler/tool/service/common
go get gitlab.com/ghostbutler/tool/signal
```

Configuration file
------------------

The configuration file `conf.json` is like so:

```json
{
	"securityManager": {
		"hostname": "securitymanager.ghostbutler",
		"username": "GhostButler2018Scanner",
		"password": "GhostButlerProject2018*"
	}
}
```

|Name|Type|Description|
|----|----|-----------|
|securityManager.hostname|string|This is the hostname of the security manager. by default, securitymanager.ghostbutler|
|securityManager.username|string|The username to request an API key to security manager|
|securityManager.password|string|The password to request an API key to security manager|

Detection rule
--------------

A detection rule is a way to specify how the scanner will certify that a service is, or not,
of a certain type.

The detection rules specification are like so in code

```go
type DetectionRule struct {
	// rule name
	Name string `json:"name"`

	// the port to do request on
	Port int `json:"port"`

	// the path to request
	Path string `json:"path"`

	// http method
	Method string `json:"method"`

	// does request use HTTPS?
	IsHTTPS bool `json:"isHTTPS"`

	// data to be sent (can be empty)
	Data string `json:"data"`

	// possible answers
	PossibleAnswer [ ]struct {
		// the response code to consider
		ResponseCode int `json:"responseCode"`

		// header analysis (if more then one, all headers conditions must be true)
		IsMustAnalyzeHeader bool `json:"isMustAnalyzeHeader"`
		HeaderAnalysis [ ]struct {
			// the header name
			Name string `json:"name"`

			// the expected value (base64 format to do not have problem with special character such as ")
			ExpectedValue string `json:"expectedValue"`

			// must be equal? if not it must only contains the ExpectedValue
			IsMustBeEqual bool `json:"isMustBeEqual"`
		} `json:"validHeader"`

		// content analysis (if more then one, at least one one the content expectation has to be true)
		IsMustAnalyzeContent bool `json:"isMustAnalyzeContent"`
		ContentAnalysis [ ]struct{
			// the expected value (base64 format to do not have problem with special character such as ")
			ExpectedValue string `json:"expectedValue"`

			// must be equal? if not it must only contains the ExpectedValue
			IsMustBeEqual bool `json:"isMustBeEqual"`
		} `json:"validContent"`
	} `json:"possibleAnswer"`
}
```

The `expectedValue` must be **base64** formatted to handle special characters such as '"' that
would be allowed in json value.

Here is an example for mFi device detection rules:

```json
{
	"name": "mfi",
	"port": 80,
	"path": "/mfi/ping.cgi",
	"method": "GET",
	"isHTTPS": false,
	"data": "",
	"possibleAnswer": [
		{
			"responseCode": 200,
			"isMustAnalyzeContent": true,
			"validContent": [
				{
					"expectedValue": "eyJyc3AiOiJhY2sifQ==",
					"isMustBeEqual": false
				}
			]
		},
		{
			"responseCode": 302,
			"isMustAnalyzeHeader": true,
			"validHeader": [
				{
					"name": "Cookie",
					"expectedValue": "QUlST1NfU0VTU0lPTklEPQ==",
					"isMustBeEqual": false
				},
				{
					"name": "Location",
					"expectedValue": "L2Nvb2tpZWNoZWNrZXI/dXJpPS9tZmkvcGluZy5jZ2k=",
					"isMustBeEqual": true
				}
			]
		}
	]
}
```

This json sample describes the way to detect a mFi device by the following rules:

  A GET request will be made for URL `/mfi/ping.cgi` on port 80 of current scanned IP.
  The response will then be analyzed. If:
  
  - The response code is 200
  
    and
    
    The data of the response is equal to `{"rsp":"ack"}`
  
  OR
  
  - The response code is 302
  
    and
    
    The header `Cookie` of the response contains `AIROS_SESSIONID=`
    
    and
    
    The header `Location` of the response equals `/cookiechecker?uri=/mfi/ping.cgi`

  then the current IP will be registered as a mFi outlet device and the IP will then be
  sent to mFi controller.

Another example with the rule to detect a philips HUE hub:

```json
{
	"name": "hue",
	"port": 80,
	"path": "/api/config",
	"method": "GET",
	"isHTTPS": false,
	"data": "Im5hbWUiOiJQaGlsaXBzIGh1ZSI=",
	"possibleAnswer": [
		{
			"responseCode": 200,
			"isMustAnalyzeContent": true,
			"validContent": [
				{
					"expectedValue": "",
					"isMustBeEqual": false
				}
			]
		}
	]
}
```

This was the definition of a detection rule. But when a controller register a detection rule to the scanner,
it also has to specify where to send the scanning results.

To do that, use the following structure:

```go
type DetectionOrder struct {
	// detection rule
	Rule DetectionRule `json:"rule"`

	// controller data
	Controller struct {
		// controller IP
		IP string `json:"ip"`

		// controller listening port
		Port int `json:"port"`

		// controller name
		Name string `json:"name"`

		// URL path where to send the detected IP
		Path string `json:"path"`
	} `json:"controller"`
}
```

Where rule is the previous structure, and controller, the way to contact the controller when results are found.

Example:

```json
{
  "rule": { "PREVIOUS RULE-": "-BODY" },
  "controller": {
    "port": 16561,
    "path": "/api/v1/controller/add"
  }
}
```

HUE:

```json
{
	"rule": {
		"name": "hue",
		"port": 80,
		"path": "/api/config",
		"method": "GET",
		"isHTTPS": false,
		"data": "",
		"possibleAnswer": [
			{
				"responseCode": 200,
				"isMustAnalyzeContent": true,
				"validContent": [
					{
						"expectedValue": "Im5hbWUiOiJQaGlsaXBzIGh1ZSI=",
						"isMustBeEqual": false
					}
				]
			}
		]
	},
	"controller": {
		"port": 16563,
		"path": "/api/v1/controller/add"
	}
}
```

Both `ip` and `name` (controller ip/name) are auto-filled by scanner when requesting to security manager
details about controller.

Scan result
-----------

Once scan is finished, results are sent back to the controller, with this form:

```json
{
  "devices": [
    "192.168.0.1", "10.0.0.3"
  ]
}
```

For example.

Endpoint
--------

- POST /api/v1/scanner/register

  Register a new detection rule associated to a controller.
  
  Header data:
  
    - X-Ghost-Butler-Key:

      The one time key you got from security manager to perform this request.

  Body data:
  
    The content of this request is a json structure such this one:
    
```json
{
	"rule": {
		"name": "mfi",
		"port": 80,
		"path": "/mfi/ping.cgi",
		"method": "GET",
		"isHTTPS": false,
		"data": "",
		"possibleAnswer": [
			{
				"responseCode": 200,
				"isMustAnalyzeContent": true,
				"validContent": [
					{
						"expectedValue": "eyJyc3AiOiJhY2sifQ==",
						"isMustBeEqual": false
					}
				]
			},
			{
				"responseCode": 302,
				"isMustAnalyzeHeader": true,
				"validHeader": [
					{
						"name": "Cookie",
						"expectedValue": "QUlST1NfU0VTU0lPTklEPQ==",
						"isMustBeEqual": false
					},
					{
						"name": "Location",
						"expectedValue": "L2Nvb2tpZWNoZWNrZXI/dXJpPS9tZmkvcGluZy5jZ2k=",
						"isMustBeEqual": true
					}
				]
			}
		]
	},
	"controller": {
		"port": 16561,
		"path": "/api/v1/controller/add"
	}
}
```

*Both `rule` and `controller` part of this structure have been described above.*

Author
------

Ghost Butler <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/service/scanner.git


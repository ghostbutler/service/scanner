package service

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"io"
	"io/ioutil"
	"math"
	"net"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	ScannerTimeout = time.Second * 8
)

type ControllerResult struct {
	// controller details
	Controller *Controller

	// result
	ScannerResult []common.ScannerResult
}

// the scanner management structure
type Scanner struct {
	// scanner thread
	ThreadCount int

	// is running?
	IsRunning bool

	// detection rules
	detectionRuleMutex sync.Mutex
	detectionRule      map[string]*DetectionOrder

	// result
	resultMutex sync.Mutex
	result      map[string]*ControllerResult

	// http client
	httpClient *http.Client

	// key manager instance
	keyManager *keystore.KeyManager
}

// build scanner
func BuildScanner(keyManager *keystore.KeyManager) *Scanner {
	// build scanner
	scanner := &Scanner{
		detectionRule: make(map[string]*DetectionOrder),
		ThreadCount:   ScannerThreadCount,
		IsRunning:     true,
		keyManager:    keyManager,
	}

	// build http(s) client which accepts insecure certificate
	scanner.httpClient = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
		Timeout: ScannerTimeout,
	}

	// start scanner thread
	go scanner.scanManagementThread()

	// ok
	return scanner
}

// find interesting interfaces
func (scanner *Scanner) findInterface() []*NIC {
	// build output
	output := make([]*NIC,
		0,
		1)

	// get interfaces list
	interfaceList, _ := net.Interfaces()

	// iterate interfaces
	for _, _interface := range interfaceList {
		// the interface is authorized by default
		isInterfaceAuthorized := true

		// check filter
		for _, filter := range InterfaceFilter {
			// compile regexp
			re, _ := regexp.Compile(filter)

			// match filter?
			if re.Match([]byte(_interface.Name)) {
				isInterfaceAuthorized = false
				break
			}
		}

		// is this interface authorized?
		if isInterfaceAuthorized {
			// get addresses associated to this interface
			addressList, _ := _interface.Addrs()

			// iterate addresses
			for _, address := range addressList {
				// cut address
				if splitAddress := strings.Split(address.String(),
					"/"); len(splitAddress) == 2 {
					// build ip (only for ipv4 for now)
					if ipPartRaw := strings.Split(splitAddress[0], "."); len(ipPartRaw) == 4 {
						// ip parts
						ipPart := [4]int{}

						// convert to int
						for i := 0; i < 4; i++ {
							ipPart[3-i], _ = strconv.Atoi(ipPartRaw[i])
						}

						// mask
						mask, _ := strconv.Atoi(splitAddress[1])

						// build NIC
						nic := &NIC{
							Name:    _interface.Name,
							Address: ipPart[0] | ((ipPart[1] << 8) & 0x0000FF00) | ((ipPart[2] << 16) & 0x00FF0000) | ((ipPart[3] << 24) & 0xFF000000),
							Mask:    mask,
						}

						// add to output
						output = append(output,
							nic)
					}
				}
			}
		}

	}

	// ok
	return output
}

// interface range definition
type InterfaceRange struct {
	// first ip
	FirstIP int

	// ip count
	IPCount int

	// NIC instance
	Nic *NIC
}

type NICInterfaceRange struct {
	// the range for each thread
	Range []*InterfaceRange

	// the nic
	Nic *NIC
}

// divide all NIC into scanning range according to threads count
// output [ Interface ][ ScanningRangeForThread ]*InterfaceRange
func (scanner *Scanner) divideScanningRange(nicList []*NIC) []NICInterfaceRange {
	// allocate scanning range
	output := make([]NICInterfaceRange,
		len(nicList))

	// iterate NIC
	for nicIndex, nic := range nicList {
		// calculate total ip count
		var totalIPCount int
		if nic.Mask >= 32 {
			totalIPCount = 1
		} else {
			totalIPCount = int(math.Pow(2,
				float64(32-nic.Mask))) - 2
		}

		// current IP (correct if address is not matching the mask)
		currentIP := (nic.Address & ^(totalIPCount + 1)) + 1

		// save nic
		output[nicIndex].Nic = nic

		// iterate threads scanning range
		LastIP := currentIP + totalIPCount
		for i := 0; i < scanner.ThreadCount; i++ {
			// build interface range
			interfaceRange := &InterfaceRange{
				Nic:     nic,
				FirstIP: currentIP,
			}

			// calculate ip count to scan
			if i < (totalIPCount % scanner.ThreadCount) {
				interfaceRange.IPCount = (totalIPCount / scanner.ThreadCount) + 1
			} else {
				interfaceRange.IPCount = totalIPCount / scanner.ThreadCount
			}

			// increment next first IP
			currentIP += interfaceRange.IPCount

			// add to output
			output[nicIndex].Range = append(output[nicIndex].Range,
				interfaceRange)

			// check current ip
			if currentIP >= LastIP {
				break
			}
		}
	}

	// ok
	return output
}

// add rule to rules list
func (scanner *Scanner) addRule(rule *DetectionOrder) {
	// lock rules
	scanner.detectionRuleMutex.Lock()
	defer scanner.detectionRuleMutex.Unlock()

	// build key to identify this rule
	key := rule.Controller.IP +
		":" +
		strconv.Itoa(rule.Controller.Port)

	// check if already present
	for k, r := range scanner.detectionRule {
		if k == key {
			// update time
			fmt.Println("now updating", r.Rule.Name, "rule (", key, ")")
			r.lastUpdate = time.Now()
			return
		}
	}

	// add to list
	fmt.Println("now adding", rule.Rule.Name, "rule (", key, ")")
	scanner.detectionRule[key] = rule
}

// detection management thread
func (scanner *Scanner) scanManagementThread() {
	for scanner.IsRunning {
		// delay
		time.Sleep(time.Second)

		// duplicate scanning rules
		scanner.detectionRuleMutex.Lock()
		detectionOrder := make(map[string]*DetectionOrder)
		for key, rule := range scanner.detectionRule {
			detectionOrder[key] = rule
		}
		scanner.detectionRuleMutex.Unlock()

		// check detection rules count
		if len(detectionOrder) <= 0 {
			continue
		}

		// interfaces
		fmt.Println("scan is starting on:")
		interfaceList := scanner.findInterface()
		for _, _interface := range interfaceList {
			fmt.Println("interface",
				_interface.Name,
				"with CIDR",
				strconv.Itoa((_interface.Address&0xFF000000)>>24)+
					"."+
					strconv.Itoa((_interface.Address&0x00FF0000)>>16)+
					"."+
					strconv.Itoa((_interface.Address&0x0000FF00)>>8)+
					"."+
					strconv.Itoa(_interface.Address&0x000000FF)+
					"/"+
					strconv.Itoa(_interface.Mask))
		}
		fmt.Println("now processing...")

		// divide scanning range
		interfaceRange := scanner.divideScanningRange(interfaceList)

		// count how many threads will be created
		var totalThreadCount = 0
		for _, ir := range interfaceRange {
			totalThreadCount += len(ir.Range)
		}

		// create threads
		waitGroup := sync.WaitGroup{}
		waitGroup.Add(totalThreadCount)
		scanner.result = make(map[string]*ControllerResult)
		for interfaceIndex := 0; interfaceIndex < len(interfaceList); interfaceIndex++ {
			for _, _range := range interfaceRange[interfaceIndex].Range {
				go scanner.detect(_range,
					detectionOrder,
					&waitGroup)
			}
		}

		// wait for detection completion
		waitGroup.Wait()

		// send results to controllers
		scanner.shareResult()

		// check rules lifetime
		scanner.detectionRuleMutex.Lock()
		for key, rule := range scanner.detectionRule {
			if !rule.IsPermanent &&
				time.Now().Sub(rule.lastUpdate) >= DetectionRuleRemovalDelay {
				fmt.Println("now removing",
					key,
					"detection rule (",
					rule.Rule.Name,
					")")
				delete(scanner.detectionRule,
					key)
			}
		}
		scanner.detectionRuleMutex.Unlock()
	}
}

// detection scan thread
func (scanner *Scanner) detect(interfaceRange *InterfaceRange,
	order map[string]*DetectionOrder,
	waitGroup *sync.WaitGroup) {
	defer waitGroup.Done()
	LastIP := interfaceRange.FirstIP + interfaceRange.IPCount
	for ip := interfaceRange.FirstIP; ip < LastIP; ip++ {
		// build ip
		ipText := strconv.Itoa((ip&0xFF000000)>>24) +
			"." +
			strconv.Itoa((ip&0x00FF0000)>>16) +
			"." +
			strconv.Itoa((ip&0x0000FF00)>>8) +
			"." +
			strconv.Itoa(ip&0x000000FF)

		// iterate rules
		for _, rule := range order {
			ruleKey := rule.Controller.IP +
				":" +
				strconv.Itoa(rule.Controller.Port)
			if scanner.testRuleOnIP(ipText,
				&rule.Rule) {
				scanner.resultMutex.Lock()
				if _, isExist := scanner.result[ruleKey]; !isExist {
					scanner.result[ruleKey] = &ControllerResult{
						Controller: &rule.Controller,
					}
				}
				scanner.result[ruleKey].ScannerResult = append(scanner.result[ruleKey].ScannerResult,
					common.ScannerResult{
						IP:       ipText,
						RuleName: rule.Rule.Name,
					})
				fmt.Println("found",
					ipText,
					"matching rule",
					rule.Rule.Name)
				scanner.resultMutex.Unlock()
			}
		}
	}
}

// send result
func (scanner *Scanner) sendResult(controllerResult *ControllerResult) {
	// body
	export := common.ScannerResultList{
		Result: make([]common.ScannerResult,
			0,
			1),
	}
	for _, result := range controllerResult.ScannerResult {
		export.Result = append(export.Result,
			common.ScannerResult{
				IP:       result.IP,
				RuleName: result.RuleName,
			})
	}
	bodyContent, _ := json.Marshal(export)
	body := bytes.NewBuffer(bodyContent)

	// build url
	url := "https://" +
		controllerResult.Controller.IP +
		":" +
		strconv.Itoa(controllerResult.Controller.Port) +
		controllerResult.Controller.Path

	// build request
	request, _ := http.NewRequest("POST",
		url,
		body)

	// get one time use token
	if oneTimeUseToken, err := scanner.keyManager.GetOneUseKey(); err == nil {
		request.Header.Add(keystore.OneTimeKeyShareHeaderName,
			oneTimeUseToken.PublicKey)
		if response, err := scanner.httpClient.Do(request); err == nil {
			defer common.Close(response.Body)
			_, _ = io.Copy(ioutil.Discard,
				response.Body)
			if response.StatusCode != http.StatusOK {
				fmt.Println("result(s) were not accepted by",
					controllerResult.Controller.IP,
					"(bad one-time key?)")
			}
		} else {
			fmt.Println("couldn't send result(s) to",
				controllerResult.Controller.IP,
				":",
				err)
		}
	} else {
		fmt.Println("couldn't get one time API key...")
	}
}

// share result to controllers
func (scanner *Scanner) shareResult() {
	fmt.Println("now sharing results to controllers...")
	for _, controllerResult := range scanner.result {
		scanner.sendResult(controllerResult)
	}
}
